var annotated_dup =
[
    [ "closedLoop", null, [
      [ "closedLoop", "classclosedLoop_1_1closedLoop.html", "classclosedLoop_1_1closedLoop" ]
    ] ],
    [ "controllerTsk", null, [
      [ "controllerTask", "classcontrollerTsk_1_1controllerTask.html", "classcontrollerTsk_1_1controllerTask" ]
    ] ],
    [ "encoder2", null, [
      [ "Encoder", "classencoder2_1_1Encoder.html", "classencoder2_1_1Encoder" ]
    ] ],
    [ "encoder3", null, [
      [ "Encoder", "classencoder3_1_1Encoder.html", "classencoder3_1_1Encoder" ]
    ] ],
    [ "motor", null, [
      [ "MotorDriver", "classmotor_1_1MotorDriver.html", "classmotor_1_1MotorDriver" ]
    ] ],
    [ "simonTask", null, [
      [ "simonTask", "classsimonTask_1_1simonTask.html", "classsimonTask_1_1simonTask" ]
    ] ],
    [ "task1", null, [
      [ "controllerTask", "classtask1_1_1controllerTask.html", "classtask1_1_1controllerTask" ]
    ] ],
    [ "task2", null, [
      [ "uiTask", "classtask2_1_1uiTask.html", "classtask2_1_1uiTask" ]
    ] ],
    [ "UI_data1", null, [
      [ "UI_data", "classUI__data1_1_1UI__data.html", "classUI__data1_1_1UI__data" ]
    ] ],
    [ "UI_data3", null, [
      [ "uiTask", "classUI__data3_1_1uiTask.html", "classUI__data3_1_1uiTask" ]
    ] ]
];