var files_dup =
[
    [ "closedLoop.py", "closedLoop_8py.html", [
      [ "closedLoop", "classclosedLoop_1_1closedLoop.html", "classclosedLoop_1_1closedLoop" ]
    ] ],
    [ "controllerTsk.py", "controllerTsk_8py.html", [
      [ "controllerTask", "classcontrollerTsk_1_1controllerTask.html", "classcontrollerTsk_1_1controllerTask" ]
    ] ],
    [ "elevator_fsm.py", "elevator__fsm_8py.html", "elevator__fsm_8py" ],
    [ "encoder2.py", "encoder2_8py.html", [
      [ "Encoder", "classencoder2_1_1Encoder.html", "classencoder2_1_1Encoder" ]
    ] ],
    [ "encoder3.py", "encoder3_8py.html", [
      [ "Encoder", "classencoder3_1_1Encoder.html", "classencoder3_1_1Encoder" ]
    ] ],
    [ "fibonacci.py", "fibonacci_8py.html", "fibonacci_8py" ],
    [ "main1.py", "main1_8py.html", "main1_8py" ],
    [ "main2.py", "main2_8py.html", "main2_8py" ],
    [ "main3.py", "main3_8py.html", "main3_8py" ],
    [ "motor.py", "motor_8py.html", "motor_8py" ],
    [ "nucleo_LED.py", "nucleo__LED_8py.html", "nucleo__LED_8py" ],
    [ "shares2.py", "shares2_8py.html", "shares2_8py" ],
    [ "shares3.py", "shares3_8py.html", "shares3_8py" ],
    [ "simonSays_main.py", "simonSays__main_8py.html", "simonSays__main_8py" ],
    [ "simonTask.py", "simonTask_8py.html", [
      [ "simonTask", "classsimonTask_1_1simonTask.html", "classsimonTask_1_1simonTask" ]
    ] ],
    [ "task1.py", "task1_8py.html", [
      [ "controllerTask", "classtask1_1_1controllerTask.html", "classtask1_1_1controllerTask" ]
    ] ],
    [ "task2.py", "task2_8py.html", [
      [ "uiTask", "classtask2_1_1uiTask.html", "classtask2_1_1uiTask" ]
    ] ],
    [ "termPage.py", "termPage_8py.html", null ],
    [ "UI_data1.py", "UI__data1_8py.html", [
      [ "UI_data", "classUI__data1_1_1UI__data.html", "classUI__data1_1_1UI__data" ]
    ] ],
    [ "UI_data3.py", "UI__data3_8py.html", [
      [ "uiTask", "classUI__data3_1_1uiTask.html", "classUI__data3_1_1uiTask" ]
    ] ],
    [ "UI_front1.py", "UI__front1_8py.html", "UI__front1_8py" ],
    [ "UI_front2.py", "UI__front2_8py.html", "UI__front2_8py" ],
    [ "UI_front3.py", "UI__front3_8py.html", "UI__front3_8py" ]
];