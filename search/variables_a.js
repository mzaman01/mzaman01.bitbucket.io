var searchData=
[
  ['new_5fkp_249',['new_Kp',['../classclosedLoop_1_1closedLoop.html#a1cde3a32e9fc89493a8cca78679936de',1,'closedLoop::closedLoop']]],
  ['newposition_250',['newPosition',['../classencoder2_1_1Encoder.html#ac5e3b52eaa9d7ee53e959bdcfc4ce404',1,'encoder2.Encoder.newPosition()'],['../classencoder3_1_1Encoder.html#aa88f62f63c5ac5457b5de42a4de75e6b',1,'encoder3.Encoder.newPosition()']]],
  ['nexttime_251',['nextTime',['../classcontrollerTsk_1_1controllerTask.html#a12e62aa96051e06bafb571d2bdf18c27',1,'controllerTsk.controllerTask.nextTime()'],['../classtask1_1_1controllerTask.html#a12d037498ad5973c8b3e9502758a34fb',1,'task1.controllerTask.nextTime()']]],
  ['notpress_252',['notPress',['../classsimonTask_1_1simonTask.html#a13f1c4e8757a75cd422a61c158898892',1,'simonTask::simonTask']]],
  ['now_253',['now',['../classsimonTask_1_1simonTask.html#a14b13d14bde50c7868431dab77057399',1,'simonTask.simonTask.now()'],['../classtask2_1_1uiTask.html#aaa2344862b5534d9162a41f5eab05a44',1,'task2.uiTask.now()'],['../classUI__data1_1_1UI__data.html#a23366da57350f42afb96e7711f9ddc40',1,'UI_data1.UI_data.now()'],['../classUI__data3_1_1uiTask.html#a37e9789a9c89c9293153758f7153fd29',1,'UI_data3.uiTask.now()']]],
  ['nsleep_5fpin_254',['nSLEEP_pin',['../classmotor_1_1MotorDriver.html#af183e4c1add4df60bd323c223e95e0d2',1,'motor::MotorDriver']]],
  ['num1_255',['num1',['../UI__front1_8py.html#a9bb91a13666a969adc2f8d47eb843f2c',1,'UI_front1.num1()'],['../UI__front2_8py.html#a6d0cc2e555eab1522097fe55ea507688',1,'UI_front2.num1()'],['../UI__front3_8py.html#a342a91612d69d621075c432cbf60c03a',1,'UI_front3.num1()']]]
];
