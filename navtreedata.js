/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "My Project", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Fibonacci Calculator", "index.html#sec_fib", null ],
    [ "Elevator FSM", "index.html#sec_elevator", null ],
    [ "Nucleo Board LED Patterns", "index.html#sec_LED", null ],
    [ "Simon Says Nucleo Game", "index.html#sec_SimonSays", null ],
    [ "UI Term Project", "index.html#sec_UIproject", null ],
    [ "ME 305 UI Term Project", "page1.html", [
      [ "ME 305 UI-Motor Term Project", "page1.html#sec", [
        [ "Week 1: Extending Your Interface", "page1.html#subsection1", null ],
        [ "Week 2: Incremental Encoders", "page1.html#subsection2", null ],
        [ "Week 3: DC Motor Control", "page1.html#subsection3", null ],
        [ "Week 4: Reference Tracking", "page1.html#subsection4", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"UI__data1_8py.html",
"main3_8py.html#a978dcb3e6c828f821e30d871283ecbd8"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';